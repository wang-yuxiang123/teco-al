#ifndef UAL_COM_LOG_H_
#define UAL_COM_LOG_H_

#include <cstdio>

#define __LOG_INFO_PREFIX "INFO"
#define __LOG_WARN_PREFIX "WARNING"
#define __LOG_ERR_PREFIX "ERROR"

#define LOG(format, ...)                                                                  \
    do {                                                                                  \
        printf("%s [%s %d]: => " format " \n", __LOG_INFO_PREFIX, __FUNCTION__, __LINE__, \
               ##__VA_ARGS__);                                                            \
    } while (0)

#define WARNING(format, ...)                                                              \
    do {                                                                                  \
        printf("%s [%s %d]: => " format " \n", __LOG_WARN_PREFIX, __FUNCTION__, __LINE__, \
               ##__VA_ARGS__);                                                            \
    } while (0)

#define ERROR(format, ...)                                                              \
    do {                                                                                \
        printf("%s [%s %d]: %s => " format " \n", __LOG_ERR_PREFIX, __FILE__, __LINE__, \
               __FUNCTION__, ##__VA_ARGS__);                                            \
    } while (0)

#define ERROR_IF(cond, format, ...)         \
    do {                                    \
        if (cond) {                         \
            ERROR(format, ##__VA_ARGS__);   \
            return TECOAL_STATUS_BAD_PARAM; \
        }                                   \
    } while (0)

#define ERROR_IF_NOT(cond, log, ...) ERROR_IF(!(cond), log, ##__VA_ARGS__)

#endif  // UAL_COM_LOG_H_
