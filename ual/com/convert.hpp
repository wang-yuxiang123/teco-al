// BSD 3- Clause License Copyright (c) 2024, Tecorigin Co., Ltd. All rights
// reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// Neither the name of the copyright holder nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY,OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)  ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
// OF SUCH DAMAGE.

#ifndef UAL_COM_CONVERT_HPP_
#define UAL_COM_CONVERT_HPP_

#include "ual/com/def.h"

namespace tecoal {
namespace ual {
namespace common {

static int inline convertAlgoToIndex(UALAlgoType type) {
    switch (type) {
        case UALAlgoType::UAL_ALGO_0: return 0;
        case UALAlgoType::UAL_ALGO_1: return 1;
        case UALAlgoType::UAL_ALGO_2: return 2;
        case UALAlgoType::UAL_ALGO_3: return 3;
        case UALAlgoType::UAL_ALGO_4: return 4;
        case UALAlgoType::UAL_ALGO_5: return 5;
        case UALAlgoType::UAL_ALGO_6: return 6;
        default: {
            throw std::runtime_error("UALAlgoType is not exist!\n");
        }
    }
}
}  // namespace common
}  // namespace ual
}  // namespace tecoal

#endif  // UAL_COM_CONVERT_HPP_
