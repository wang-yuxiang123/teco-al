if [ ! -d "./thirdparty" ]; then
    mkdir thirdparty
fi

if [ ! -d "./thirdparty/protobuf" ]; then
    if [ ! -d "./thirdparty/protobuf-3.21.8" ]; then
        cd ./thirdparty
        cp /eco/thirdparty/protobuf-3.21.8.tar.gz .
        tar -xf protobuf-3.21.8.tar.gz
        cd ..
    fi
    cd ./thirdparty/protobuf-3.21.8
    rm -rf build
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=../../protobuf -Dprotobuf_BUILD_TESTS=OFF ..
    make -j32
    make install
    cd ../../..
fi

if [ ! -d "./thirdparty/googletest" ]; then
    if [ ! -d "./thirdparty/googletest-release-1.12.1" ]; then
        cd ./thirdparty
        cp /eco/thirdparty/googletest-release-1.12.1.tar.gz .
        tar -xf googletest-release-1.12.1.tar.gz
        cd ..
    fi
    cd ./thirdparty/googletest-release-1.12.1
    rm -rf build
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=../../googletest -Dprotobuf_BUILD_TESTS=OFF ..
    make -j32
    make install
    cd ../../..
fi

if [ ! -d "./thirdparty/protobuf" ]; then
    if [ ! -d "./thirdparty/protobuf-3.21.8" ]; then
        cd ./thirdparty
        cp /eco/thirdparty/protobuf-3.21.8.tar.gz .
        tar -xf protobuf-3.21.8.tar.gz
        cd ..
    fi
    cd ./thirdparty/protobuf-3.21.8
    rm -rf build
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=../../protobuf -Dprotobuf_BUILD_TESTS=OFF ..
    make -j32
    make install
    cd ../../..
fi

if [ ! -d "./thirdparty/googletest" ]; then
    if [ ! -d "./thirdparty/googletest-release-1.12.1" ]; then
        cd ./thirdparty
        cp /eco/thirdparty/googletest-release-1.12.1.tar.gz .
        tar -xf googletest-release-1.12.1.tar.g
        cd ..
    fi
    cd ./thirdparty/googletest-release-1.12.1
    rm -rf build
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=../../googletest -Dprotobuf_BUILD_TESTS=OFF ..
    make -j32
    make install
    cd ../../..
fi