import os
dep_path = "thirdparty"

deps = [
    {
        "url": "http://jfrog.tecorigin.net:80/artifactory/tecobs-tp-devlibs/protobuf/3.21.8/protobuf-3.21.8.tar.gz",
        "src": "protobuf-3.21.8",
        "build": "protobuf"
    },
    {
        "url": "http://jfrog.tecorigin.net:80/artifactory/tecobs-tp-devlibs/gtest/1.12.1/googletest-release-1.12.1.tar.gz",
        "src": "googletest-release-1.12.1",
        "build": "googletest"
    },

]

if not os.path.exists(dep_path):
    os.mkdir(dep_path)

for dep in deps:
    build = os.path.abspath(os.path.join(os.path.join(dep_path, dep["build"])))
    src = os.path.abspath(os.path.join(os.path.join(dep_path, dep["src"])))
    url = dep["url"]
    if not os.path.exists(build):
        if not os.path.exists(src):
            src_package = url.split("/")[-1]
            cmd = f"cd {dep_path} && wget {url} && tar -xf {src_package}; cd -"
            os.system(cmd)
        cmd = f"cd {src}; rm -rf build; mkdir build; cd build; cmake -DCMAKE_INSTALL_PREFIX={build} -Dprotobuf_BUILD_TESTS=OFF ..; make -j32 && make install; cd -"
        os.system(cmd)
